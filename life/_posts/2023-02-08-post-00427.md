---
layout: post
title: "올해 초 기대되는 섹터 및 종목은? Feat. 게임관련주, 중국판호"
toc: true
---

 

 안녕하세요!
 여러분들의 주린이 탈출을 돕기위한 탈출작전의 지휘자 차트충입니다.
 

 요즘같이 불확실하고 어려운 시장에서도 살아남기위해, 차트충이 열심히 핫한 섹터를 발굴해보도록 하겠습니다.
 

 자, 주린이 탈출작전 시작합니다.
 

## 1. 기업접근
  판호는 중국에서 게임이나 전적 등에 유통 허가를 낌새 주는 고유 번호다. 2017년 3월 한반도 사드(고고도미사일방어체계) 배치에 대한 보복으로 비공식적 한한령이 내려지면서 중국 당국은 내국 게임업체에 문을 닫았다.
 

 기사에 따르면, 중국 당국은 석세 8월 “온라인 게임은 정서적 아편”이라며 경합 공 전체를 대대적으로 단속했다. 이출 왜냐하면 텐센트, 넷이즈 등 중국 메이저 게임사들이 큰 타격을 입었다. 그럼에도 불구하고 ‘제로 코로나’ 정책에 중국 경기가 추락하자 당국은 전달 텐센트 게임의 판호를 승인했다. 이때부터 중국 정부가 경제를 살리고자 시합 규제를 풀 것이란 전망이 나왔다.
 

 이번 판호 발급으로 상승한 한한령 유몽 기대감은 게임주에 유입되고 게임섹터에 훈풍이 불어올 것으로 보인다. 당연히 겨우 중국은 지금까지 한한령의 사물 자체를 인정하지 않았고 판호 발급 기준조차 제시한 비교적 없어 시장 불확실성이 여전하다.
 

 금일은 이참 판호 발급으로 인해 게임주에 수급이 몰릴 것으로 예상되며 실적과 전망이 좋은 기업에 대해 경례 할 예정이다.
 

## 2. 기업소개

### < 엔씨소프트 >

  엔씨소프트는 온라인, 모바일 게임을 개발해 운영하는 기업이다. PC게임 '리니지'와 '리니지2', '아이온', '블레이드앤소울', 모바일 싸움 '리니지M', '리니지2M', '리니지W' 등이 주 게임이다. 연결대상 종속회사를 통하여, 소프트웨어 개발, 제조, 판매업 뿐만 아니라 프로야구서비스 및 콜센터 서비스를 제공하는 비즈니스 등을 영위하고 있다. 만근 들어 유니버스 등 엔터, 메타버스 상업 등으로도 실력을 키우고 있다.
 

### [ 주요 일 한계 현황 ]
 ※ 연결기준 온라인 게임은 리니지, 리니지2, 아이온, 블레이드앤소울, 길드워2의 글로벌 합계임

  엔씨소프트(NCSOFT)는 현세 모바일 및 온라인 플랫폼 기반의 중대 게임들 기수 부분유료화 방식을 통해 매출이 발생하고 있다. 스마트폰 보급 확대로 인해 기존 PC의 온라인 게임에서 모바일에서의 발매 비중이  확대되고 있다. 자네 한가운데 세일 비중을 심히 보유 하고 있는 것은 기존에 리니지M 성실 리니지2M 이었는데, 최근들어 21년도 신작인 리니지W의 매출 비중이 증가추세에 있다.

### < 리니지M >

  엔씨소프트의 리니지M은 2017년에 정식 출시하였고, 모바일 MMORPG를 이끌어 나가는 대표작으로 새로운 기술의 접목을 통해 플랫폼의 한계를 뛰어 넘고 있다는 평을 받고 있다. 모든 서버의 이용자가 같은 시공간에서 전투를 펼치는 마스터 서버, 음성만으로 특성 조종과 플레이를 실행하는 보이스 커맨드 등 장르를 선도하는 리니지M만의 전문적 도전은 계속되고 있다.

### < 리니지2M >

  엔씨소프트의 리니지2M은 2019년도에 정식출시 되었으며, 모바일 기술의 진보를 이룩하여 업계에서는 현존하는 모바일 게임의 한계를 뛰어넘고 기술력의 정수를 드러낸 모바일 게임이라 칭한다. 최고 사양의 그래픽과 물리 치돌 기술, 축원 경로 오픈 월드 등 엔씨만이 지닌 과감한 도전력과 장인정신을 바탕으로 모바일 게임의 전문적 진보를 실현시켰다는 평가를 받고 있다.

### < 리니지W >

  엔씨소프트의 리니지W은 2021년도에 정식 출시되었으며, 리니지 시리즈의 결정판이라고 볼 성명 있다. 리니즈의 특징인 쿼터뷰에 Full 3D를 더해 이전에는 입때껏 담지 못한 인물과 세계의 디테일을 아울러 담았다. 원작으로부터 150년 후의 시대를 다크 판타지로 재해석했고 세계로 확장된 리니지W는 여러 나라의 이용자가 언어의 제약 궁핍히 하나의 서버에서 함께 성장하고 경쟁 할 무망지복 있는 메리트가 있다. 이러한 세계적인 글로벌유저들과 같이 플레이를 할 행복 있는 재미가 매출향상에 큰 이바지를 하였다.
 

### [ 판매경로 및 판매방법 ]
   엔씨소프트 매출의 양반 큰 비중을 차지하는 모바일 게임의 경우는 대부분 구글과 애플의 모바일 결제 시스템을 통해서 판매가 이뤄지고 있으며, 온라인 게임은 PC 온라인 결제를 통해서 유희 이용권 및 유료 아이템 등의 판매가 이뤄지고 있다.
 

## 3. 회사 실적

#### (1) 22년 3분기 매출액(엔씨소프트)

  엔씨소프트의 22년 3분기 매출액은 묵은해 삼동 대비 20.7% 증가하였다.
 

#### (2) 22년 3분기 영업이익(엔씨소프트)

  엔씨소프트의 22년 3분기 영업이익은 과년 동문생 대비 50.0% 증가하였다.

#### (3) 22년 3분기 순이익(엔씨소프트)

  엔씨소프트의 22년 3분기 순이익은 작년 동인 예비 82.3% 증가하였다.
 

  22년도에 신작이 없었지만 전년대비 실적이 증가한 이유는 리니지W는 업데이트와 글로벌 IP 제휴 마케팅 성과로 안정적인 매출이 일어났기 때문이다. 21년 11월에 출시한 리지니W는 출시 이후 근실히 매출을 유지하며 22년 3분기 안목 총체 매출의 39%를 소유물 할 정도로 큰 인기를 누리며 호실적을 견인하고 있다. 더구나 3분기에는 북미·유럽과 아시아 시장의 발수 성장이 두드러졌다. 북미·유럽은 안해 동기대비 62%, 아시아 지역은 48% 성장했다.
 

#### [ 엔씨소프트 포괄손익계산서 ]

  엔씨소프트는 21년도에 크게 급증한 인건비와 마케팅 실비 탓에 실적이 저조했지만, 22년도에 들어서면서 복리후생비 감소와 성과연동형 인센티브가 조정되어 비용을 절감하였고 과한 마케팅 비용을 줄인 것도 실적향상에 도움을 주었다. 장사 확장과 개발자 연봉 인상으로 인한 인건비 상승이 업계 전반에 나타났고, 이를 절감하기는 어려운 상황속에서 효율적인 비용집행에 성공한 것이다. 동사의 22년도 3분기까지 누적 매출액은 상년 시근 예비 30.4% 증가 하였으며, 영업이익은 92.5% 상승, 당기순이익은 71.2% 증가 하였다.
 

  허나 일각에선 엔씨소프트는 22년 4분기 저록 감소를 피할 수명 없다고 한다. '리니지W'가 21년도 4분기에 출시되어 아들애 초엽 흥행 신기록을 세웠기 때문이다. 또 보편적으로 판매 및 영업이익이 구년 동기에 대비되어 평가된다. 22년 4분기에는 리니지W과 같은 신작이 없었으므로 오픈 효과를 발휘할 기회가 없기에 이부분이 4분기 실적이 저조할 것이라는 의견이다.

 그에 반면 엔씨소프트의 ‘길드워2’ 끝장 북미 지역서 '22년도 그해 최고의 게임'으로 인정받고 있고 인기가 높아지면서 국외 발수 성장이 더더욱 기대되는 전망도 있다. 참고로 길드워2는 엔씨 북미 스튜디오 아레나넷이 개발해 올 시중 10주년을 맞이한 대표적인 장군 MMORPG로, 글로벌 IP로서 존재감을 공고히 하고 있다. 해석 초 세 번째 확장팩 '엔드오브드래곤즈'를 출시하고 글로벌 대전 플랫폼 '스팀' 서비스를 시작하는 등 지속적인 덤 확장 중에 있다.

#### [ 엔씨소프트 회사 표기 ]

  엔씨소프트의 22년도 실적은 21년도 예비 전반적으로 향상될 것으로 보인다. 22년 4분기 매출은 저조할 것으로 예측되지만, 영업이익과 당기순이익은 새로이 5천억원대 이상으로 상승, 영업이익 및 순이익률은 20%이상, ROE도 15%이상으로 상향 전망한다.

## 4. 기업재료

### (1) 신작 TL(Throne and Liberty) 기대감 ↑

  엔씨소프트가 지난 12월 27일 잔야 10시 유튜브에서 차세대 MMORPG를 목표로 개발 군속 PC·콘솔 신작 '쓰론 앤 리버티(TL)'의 상세 내용을 공개했다. 동사의 김택진 대표는 “TL(Throne and Liberty)은 국가와 세대를 초월해 모두가 즐길 목숨 있는 다중접속역할수행게임(MMORPG)이다.” 라고 말하며 모두의 이목을 집중시킨 가운데, 1,600명이 넘는 시청자가 이를 지켜봐 해석 최상 기대작이 될 것이라는 기대감을 높였다. 허나 금번 프리뷰 영상에서 TL 출시를 그해 상반기 중이라고 확인해줬지만 구체적인 일정은 공개하지 않았다.

 

 해석 신규 기대작인 TL은 PC와 콘솔 다리 디바이스에 맞는 사용자 경험(UX)과 사용자 환경(UI)을 제공하고 글로벌 이용자의 다양한 취향을 고려한 품질 커스터마이징 기능을 구현할 것이므로 많은 유저확보에 따른 수익화구조에 관심이 쏠린다. 업계에서 TL에 주목하는 이유는 리니지W 이후 신작 공백이 이어졌던 엔씨소프트의 첫 콘솔 시합 도전작이자, 블레이드앤소울 이래 10년 만에 선보이는 PC 게임이기 때문이다. 콘솔과 PC가 양서 연동되는 크로스플랫폼으로 북미시장을 개척할 이운 있을지 관심이 모아진다. 여의도에선 디아블로4와 TL의 출시 시기가 겹쳐 다툼 구도를 그릴 것이라는 관측도 내놓는다.
 

 뿐만 아니라 엔씨소프는 'TL(Throne and Liberty)' 노지 블레이드앤소울 S(아시아향 모바일 수집형 RPG), 프로젝트R(모바일·스위치향 난투형 대전액션), 퍼즐업(글로벌 퍼즐게임), 프로젝트G 등 최대 5종 신작 게임을 대비 중에 있다. 그래서 이에 따른 신작품들에 대한 기대감이 옥려 상승모멘텀을 키우게 될 것이다.
 

### (2) 중국 판호 트리핑 및 한한령 약자 기대감 ↑

 지난 12월 29일 게임업계에 따르면 중국 국가신문출판서는 전날 120여 수장 게임을 대상으로 판호를 발급했다고 발표했다. 판호 발급대상은 넥슨, 넷마블 게다가 스마일게이트로 엔씨소프트는 이번 판호발급 대상에 속하지 않지만 같은 게임섹터로서 기대감을 받을 가능성이 높다. 2017년 3월 한반도 사드(THAAD·고고도미사일방어체계) 배치 차기 중국 정부의 '한한령'이 본격화했는데 이번 중국 정부가 한국의 과수 게임에 대해 외자판호를 발급하면서 업계에선 한한령 해제에 대한 관심이 집중되고 있다. 참고로 한한령 해제가 될경우 국내 경기 및 엔터 뿐만 아니라 뷰티산업엔 호재이다.
 

### (3) The Saudi Fourth Investment Company 의 평단

 22년 1월에서 2월 한창 엔씨소프트의 주가가 곤두박질 치던시절 사우디 투자자 The Saudi Fourth Investment Company는 장내매수를 [리니지 프리서버](https://scarfdraconian.com/life/post-00036.html) 하며 엔씨소프트의 지분을 늘려갔으며 결결이 평균취득단가는 위와 다름없이 50만원대라는 것을 볼 명맥 있다. 이승 엔씨소프트의 주가는 448,000원(22.12.29 기준) 이므로 사우디 투자자가 최근에 장내매수한 가격보다 더욱 저렴하게 매수할 행복 있는 절호의 찬스이다.

 

 참고로 5%이상 지분보유의 대주주는 하기와 같다.

## 5. 사업체 차트분석

### (1) 엔씨소프트 월봉차트

  우선 엔씨소프트 월봉차트를 보면, 21년도 실적이 20년도 가일층 수없이 저조하여 주가가 곤두박질 치다가 22년도 실적이 21년도보다는 크게 증가하여 월봉 120선부근에서 지지받고 올라섰으며 요즈음 저항선은 485,000원대로 관찰 된다. 월봉상의 Stocastic slow 는 하단구간에 있어 분할매수해도 부담가지 않는 구간이다. 후 2차 저항은 605,000원대 핸드레벨 이며 60만원 초반대까지의 매물대는 두텁지 않기에 2차 저항대까지는 무난히 도달 할 것으로 보인다.

### (2) 엔씨소프트 주봉차트

  다음으로 엔씨소프트 주봉차트를 보면 엘리어트 파동에서 상승 조정 (2)파 구간으로 보이며, 주봉 60선 돌파여부가 핵심과제이다. 22년 11월 초반부터 주봉 60선 저항을 맞고 돌파를 못하는 흐름을 보였으나 요즘 중국 판호 개방에 따른 기대감과 더불어 한한령 소아 기대감이 맞물리고 동시에 실적도 개선추이에 있기에 즉변 주봉 60선 돌파는 거리 문제일 것으로 보인다.

### (3) 엔씨소프트 일봉차트

 엔씨소프트 일봉차트를 보면 요사이 일봉 240일선에서 지지를 받는 흐름을 보이며, 지난주 중국판호 개국 기대감이 게임섹터 수급에 큰 개선효과를 불러오며 갭상승을 하였지만 윗꼬리를 남기며 상승분을 반납하였다. 그래도 아침 볼린저 밴더 중심선 및 일목균형표 기준선 아울러 일봉 20선을 이탈하는 흐름을 보이지 않고 있으며 보조지표 Stochastic slow는 저위 구간에서 K가 D를 돌파하는 구간이므로 본격적인 상승 초입구간으로 보인다. 일봉 20선 및 일목균형표 기준선 이탈여부를 연방 체크할 필요가 있다.

## 6. 결론

  최근 신작 TL이 주목받고 있는 겨를 전세계 유저 IP를 확장해 나가는 엔씨소프트의 전망이 기대되는 건 확실하다. 반면 매크로적인 이슈를 배제하기 어려운 것이 주식시장의 현실이다. 그저 악재로 작용하는 높은 고금리시대에 전세계의 경기침체에 대한 두려움이 여전한 중축 중국의 위드코로나 정책의 성공여부에 따라 전세계 경기침체 가속도가 달라진다. 우리나라도 위드코로나를 뻔쩍하면 겪고 지나간것처럼 중국도 이금 상황을 바로 겪어날 것으로 예상되지만, 중국의 춘절(1월21일~27일)이 고비가 될 것으로 보인다. 러시아와 우크라이나 전쟁이 낳은 지정학적 리스크가 아직도 해결되고 있지 않는 중간 중국 판호 개국 및 한한령 소인 기대감이 게임섹터의 전반적인 주가상승 모멘텀의 트리거가 될지 지속관찰이 필요하다.

#### <참고하면 좋은 내용>
 https://chart-worm.tistory.com/33

#### 장상 그렇듯 분할 매수, 분할 매도!
해당 글은 참고용일 뿐,

#### 모든투자의 책임은 본인에게 있습니다.

 주린이를 탈출하고 싶은 분,
 주식 대박이 나고 싶은 분,
 개별 종목 상담받고 싶은 분은
 공감과 댓글 및 구독 부탁드려요~♡
